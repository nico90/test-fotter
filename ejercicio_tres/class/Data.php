<?php

/**
 * Created by PhpStorm.
 * User: nico
 * Date: 15/01/16
 * Time: 21:51
 */
class Data
{
    private $number;
    private $color;

    private $json;

    function __construct($array = ["red","blue"], $n = 10){
        $this->color = $array;
        $this->number = $n;
        $key = rand(0,count($this->color)-1);
        $this->number = rand(1,$n);
        $this->json = ["data" =>
            [
                "number" =>$this->number,
                "color" => $this->color[$key]
            ]
        ];
    }

    function __toString()
    {
        // TODO: Implement __toString() method.
        return json_encode($this->json);
    }

}