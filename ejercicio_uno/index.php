<?php
/**
 * Created by PhpStorm.
 * User: nico
 * Date: 15/01/16
 * Time: 18:47
 */

require_once "class/Number.php";

$numb = new Number(100);
print "<pre>";
var_dump($numb->hexadecimales());
print "</pre>";

print "<pre><code>";
var_dump($numb->multiplos());
print "</code></pre>";

$style = <<<EOT
    <style>
        .primo{color:red}
    </style>
EOT;

print $style;
print "<div><ul>";
    foreach($numb->getNumbers() as $n){

        print "<li class=".$numb->esPrimo($n).">" . $n . "</li>";
    }
print "</ul></div>";