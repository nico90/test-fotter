<?php

/**
 * Created by PhpStorm.
 * User: nico
 * Date: 15/01/16
 * Time: 18:37
 */
class Number
{
    private $numbers = array();

    private $pares;
    private $impares;

    private $multiplos;

    function __construct($max,$multiplos = 4)
    {
        for($number = 0;$number <= $max; $number++){
               $this->numbers[] = $number;
        }

        $this->setType();
    }

    private function setType(){
        foreach($this->numbers as $key =>$number){
            if($number%2){
                $this->impares[] = $this->numbers[$key];
            }else{
                $this->pares[] = $this->numbers[$key];
            }
        }
    }
    /*
     * @param $type String => pares - impares
     * */
    public function hexadecimales(){

        foreach($this->pares as $numb){
            $hexa[] = dechex($numb);
        }
        return ($hexa);
    }

    public function multiplos(){
        $this->multiplos = $this->numbers;
        foreach($this->numbers as $key => $number){
            if(!fmod($number, 4)){
                $this->multiplos[$key] = "[4]";
            }
        }

        return $this->multiplos;
    }

    public function getNumbers(){
        return $this->numbers;
    }

    public function esPrimo($number){
        {
            $cont=0;
            for($i=2;$i<=$number;$i++){
                if($number%$i==0){
                    if(++$cont>1)
                        return false;
                }
            }
            return "primo";
        }
    }

}