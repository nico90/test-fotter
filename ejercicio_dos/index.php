<?php
/**
 * Created by PhpStorm.
 * User: nico
 * Date: 15/01/16
 * Time: 19:00
 */

require_once "class/RssRead.php";

$url = "http://recursos-data.buenosaires.gob.ar/ckan2/ecobici/estado-ecobici.xml";
$items = new RssRead($url);
//la variable estaciones contiene el objeto de cada estacion, tienen todos sus getter para obtener cada
// dato necesario e implementarlo.

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
<div class="container">


            <table class="table">
                <thead>
                    <tr>
                        <?php foreach($items->propiedades() as $p): ?>
                            <th><?php print $p ?></th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>

                        <?php
                        foreach($items->get_items() as $key => $estaciones):
                            print "<tr><td>" . $estaciones->getEstacionId() . "</td>" .
                                  "<td>" . $estaciones->getEstacionNombre()  . "</td>" .
                                  "<td>" . $estaciones->getBicicletaDisponibles() . "</td>" .
                                  "<td>" .  $estaciones->getLatitud() . "</td>" .
                                "<td>" . $estaciones->getLongitud() . "</td>" .
                                "<td>" . $estaciones->getLugar() . "</td>" .
                                "<td>" . $estaciones->getAnclajesTotales() . "</td>" .
                                "<td>" . $estaciones->getEstacionDisponible() . "</td>" .
                                  "<td>" . $estaciones->getAnclajesDisponibles() . "</td></tr>";
                            endforeach;
                        ?>

                </tbody>
            </table>
        </div>


</div>
</body>

</html>
