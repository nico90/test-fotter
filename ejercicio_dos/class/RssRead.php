<?php

/**
 * Created by PhpStorm.
 * User: nico
 * Date: 15/01/16
 * Time: 19:46
 */
require_once "Estacion.php";

class RssRead
{
    var $url;
    var $data;
    function __construct($url){
        $this->url;
        $this->data = implode ("", file ($url));
    }
    function __toString()
    {
        // TODO: Implement __toString() method.

        return "Devuelvo un Array con objetos de estaciones";
    }

    public function get_items (){
        preg_match_all ("/<Estacion .*>.*<\/Estacion>/xsmUi", $this->data, $matches);
        $items = array ();
        foreach ($matches[0] as $match){
            $items[] = new Estacion ($match);
        }
        return $items;
    }

    public function propiedades(){
        return array("ID","Nombre","Bicicletas Disponibles","Latitud","Logitud","Lugar","Anclajes Totales","Estacion Disponible","Anclajes Disponibles");
    }
}

?>