<?php

/**
 * Created by PhpStorm.
 * User: nico
 * Date: 15/01/16
 * Time: 19:46
 */
class Estacion
{
    private $EstacionId;
    private $EstacionNombre;
    private $BicicletaDisponibles;
    private $EstacionDisponible;
    private $Latitud;
    private $Longitud;
    private $Lugar;
    private $AnclajesTotales;
    private $AnclajesDisponibles;


    function __construct ($xml){
        $this->populate ($xml);
    }

    private function populate ($xml){
        preg_match ("/<EstacionId> (.*) <\/EstacionId>/xsmUi", $xml, $matches);
        $this->EstacionId = $matches[1];

        preg_match ("/<EstacionNombre> (.*) <\/EstacionNombre>/xsmUi", $xml, $matches);
        $this->EstacionNombre = $matches[1];

        preg_match ("/<BicicletaDisponibles> (.*) <\/BicicletaDisponibles>/xsmUi", $xml, $matches);
        $this->BicicletaDisponibles = $matches[1];

        preg_match ("/<EstacionDisponible> (.*) <\/EstacionDisponible>/xsmUi", $xml, $matches);
        $this->EstacionDisponible = $matches[1];

        preg_match ("/<Latitud> (.*) <\/Latitud>/xsmUi", $xml, $matches);
        $this->Latitud = $matches[1];

        preg_match ("/<Longitud> (.*) <\/Longitud>/xsmUi", $xml, $matches);
        $this->Longitud = $matches[1];

        preg_match ("/<Lugar> (.*) <\/Lugar>/xsmUi", $xml, $matches);
        $this->Lugar = $matches[1];

        preg_match ("/<AnclajesTotales> (.*) <\/AnclajesTotales>/xsmUi", $xml, $matches);
        $this->AnclajesTotales = $matches[1];

        preg_match ("/<AnclajesDisponibles> (.*) <\/AnclajesDisponibles>/xsmUi", $xml, $matches);
        $this->AnclajesDisponibles = $matches[1];

    }

    /**
     * @return mixed
     */
    public function getEstacionId()
    {
        return $this->EstacionId;
    }

    /**
     * @return mixed
     */
    public function getEstacionNombre()
    {
        return $this->EstacionNombre;
    }

    /**
     * @return mixed
     */
    public function getBicicletaDisponibles()
    {
        return $this->BicicletaDisponibles;
    }

    /**
     * @return mixed
     */
    public function getEstacionDisponible()
    {
        return $this->EstacionDisponible;
    }

    /**
     * @return mixed
     */
    public function getLatitud()
    {
        return $this->Latitud;
    }

    /**
     * @return mixed
     */
    public function getLongitud()
    {
        return $this->Longitud;
    }

    /**
     * @return mixed
     */
    public function getLugar()
    {
        return $this->Lugar;
    }

    /**
     * @return mixed
     */
    public function getAnclajesTotales()
    {
        return $this->AnclajesTotales;
    }

    /**
     * @return mixed
     */
    public function getAnclajesDisponibles()
    {
        return $this->AnclajesDisponibles;
    }


}