/*
  Seleccionar el nombre de todas l
  as categorías a las que fue asignado el producto de id=5
 */

SELECT category.name,
FROM category
INNER JOIN category_products
ON category.id=category_products.id WHERE category.id = 5;
