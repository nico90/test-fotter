/*
Seleccionar la suma total del stock de todos los
productos de la categoría de nombre “Accesorios”
 */


SELECT category.name,Sum(stock) AS Total FROM product
INNER JOIN category_products ON product.id = category.product.id
INNER JOIN category on category.id = category_products.category_id
WHERE category.name = "accesorios";