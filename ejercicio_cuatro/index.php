<?php
/*
 * Ejercicio 4
Crear una página web que haga una llamada AJAX desde JavaScript al archivo del ejercicio
anterior (ejercicio3.php), cada vez que se presione un botón. Por cada ejecución, mostrar el
resultado de la llamada en un cuadrado con el color de fondo de la propiedad data.color
(Ejemplo: si la llamada devuelve number: 3 y color: “red”, se deberá mostrar el número 3 sobre
un fondo rojo)

 * */
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>4</title>
    <script src="footer.js"></script>
</head>
<body>
    <button onclick="ConsumirEjercicioTres.meApreto()">APRETAME!</button>
    <div id="resultado"></div>
</body>
</html>